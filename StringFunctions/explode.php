<?php

// Description : array explode ( string $delimiter , string $string [, int $limit = PHP_INT_MAX ] )

/* Example 1
$pizza  = "piece1,piece2,piece3,piece4,piece5,piece6";
$pieces = explode(",", $pizza);
echo "<pre>";
print_r($pieces);
echo "</pre>";
echo "<hr>";
echo  $implode= implode(',',$pieces);
 * 
 */

$name ="What is your name?";
$rafat= explode(" ", $name);
$profession= "what,is,your,profession";
$engineer=explode(",",$profession);
echo "<pre>";
print_r($rafat);
echo "</pre>";

echo "<hr>";
echo "<hr>";
echo "<pre>";
var_dump($rafat);
echo "</pre>";
echo "<hr>";
echo "<hr>";
echo "<pre>";
var_export($rafat);  // this can be used as code.
echo "</pre>";
echo "<hr>";
echo "<hr>";
echo "<pre>";
var_export($engineer);  // this can be used as code.
echo "</pre>";
