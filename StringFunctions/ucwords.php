<?php

// ucwords — Uppercase the first character of each word in a string

//Description : string ucwords ( string $str [, string $delimiters = " \t\r\n\f\v" ] )
// Returns a string with the first character of each word in str capitalized, if that character is alphabetic.

$name = "shaikh mahmood";
$name1 = ucwords($name);
echo $name1;
echo "</br>";
echo "</br>";
$name2 = "SHAIKH MAHMOOD";
echo ucwords($name2);
echo "</br>";
echo "</br>";
echo strtolower($name2);
echo "</br>";
echo "</br>";
echo ucwords(strtolower($name2));
echo "</br>";
echo "</br>";
$raf="hello|world";
echo ucwords($raf);
echo "</br>";
echo "</br>";
echo ucwords($raf,"|");



