<?php

// substr_count — Count the number of substring occurrences

// Description : int substr_count ( string $haystack , string $needle [, int $offset = 0 [, int $length ]] )

$text= "This is my favourite food.";
echo strlen($text);
echo "<hr>";
echo substr_count($text,"is");
echo "<hr>";
echo substr_count($text,"is",3);
echo "<hr>";
echo substr_count($text,"is",7,5);
echo "<hr>";
echo substr_count($text,"food",7);








