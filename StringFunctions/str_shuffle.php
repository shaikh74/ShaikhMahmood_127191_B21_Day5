<?php
/*str_shuffle — Randomly shuffles a string
* Description :
string str_shuffle ( string $str )
str_shuffle() shuffles a string. One permutation of all possible is created.

Parameters :
str
The input string.
Return Values :
Returns the shuffled string.

 */

$str= "What is your name?";
$shuffled= str_shuffle($str);
echo $shuffled;

echo "<hr>";
echo "<hr>";
       
$str1='rafat';
$shuffled1=str_shuffle($str1);
echo $shuffled1;

