<?php

// The rtrim() function removes whitespace or other predefined characters from the right side of a string.

$str= "Hello World!   ";
echo "Without rtrim:".$str;
echo "<br>";
echo "<br>";
echo "With rtrim:".rtrim($str);

?>
