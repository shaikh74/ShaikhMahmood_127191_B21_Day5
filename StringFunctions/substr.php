<?php
// substr — Return part of a string
//Description : string substr ( string $string , int $start [, int $length ] )

$rest1 = substr("abcdef", -1);    // returns "f"
$rest2 = substr("abcdef", -2);    // returns "ef"
$rest3 = substr("abcdef", -3, 1); // returns "d"
$rest4 = substr("abcdef", 0, -1);  // returns "abcde"
$rest5 = substr("abcdef", 2, -1);  // returns "cde"
$rest6 = substr("abcdef", 4, -4);  // returns false
$rest7 = substr("abcdef", -3, -1); // returns "de"
$rest8 =substr('abcdef', 1);     // bcdef
$rest9= substr('abcdef', 1, 3);  // bcd
$rest10= substr('abcdef', 0, 4);  // abcd
$rest11= substr('abcdef', 0, 8);  // abcdef
$rest12= substr('abcdef', -1, 1); // f

echo $rest1;
echo "<hr>";
echo $rest2;
echo "<hr>";
echo $rest3;
echo "<hr>";
echo $rest4;
echo "<hr>";
echo $rest5;
echo "<hr>";
echo $rest6;
echo "<hr>";
echo $rest7;
echo "<hr>";
echo $rest8;
echo "<hr>";
echo $rest9;
echo "<hr>";
echo $rest10;
echo "<hr>";
echo $rest11;
echo "<hr>";
echo $rest12;
echo "<hr>";
