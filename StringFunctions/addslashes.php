<?php
// Description: string addslashes ( string $str )
// Returns a string with backslashes before characters that need to be escaped. 
// These characters are single quote ('), double quote ("), backslash (\) and NUL (the NULL byte).


// Parameters: str, The string to be escaped.

//Return Values :Returns the escaped string.

$str ="What is your name?";

echo addslashes($str); 



?>


