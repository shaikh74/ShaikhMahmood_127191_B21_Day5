<?php
// implode — Join array elements with a string

//Description :
/*string implode ( string $glue , array $pieces )
  string implode ( array $pieces )
 Join array elements with a glue string.

 * Note:
implode() can, for historical reasons, accept its parameters in either order. For consistency with explode(), however, it may be less confusing to use the documented order of arguments.
Parameters :
 * glue
 *Defaults to an empty string.
pieces:
The array of strings to implode.
Return Values :
Returns a string containing a string representation of all the array elements in the same order, with the glue string between each element.

 */


$data=array("name","profession","email");
$implode=implode(" ",$data);
echo $implode;
echo "<hr>";
echo "<hr>";
$raf=explode(" ",$implode);
var_dump($raf);



