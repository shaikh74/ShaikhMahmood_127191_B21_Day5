<?php
# When a function is written inside a class, it is called method.
class MyClass{
    public function MyMethod(){
        return "The name of this method is ".__METHOD__;
    }
}

$smart=new MyClass();
echo $smart->MyMethod(); # if we want to access into any method from any class
# then we use -> this sign.
echo "<hr>";
$obj1=new MyClass();
echo $obj1->MyMethod();





