<?php 

// It is possible to specify the key only for some elements and leave it out for others:

 # Example: Keys not on all elements

$array = array(
    "a","b",8=>"c","d"
);

var_dump($array);

// In the output, you  will see the last value "d" was assigned the key 9. 
// This is because the largest integer key before that was 8.

?>



