<?php 

# Accessing array elements

$rafat= array(
    
    "foo" => "bar",
    42 => 24,
    "multi" => array(
        "rafat" => 4,
        "dimensional" => array(
           "array" => "foo",
           "back"=> "square",
        )
        )
    
);

var_dump ($rafat);
echo "</br>";
echo "</br>";
var_dump ($rafat["foo"]);
echo "</br>";
echo "</br>";
var_dump($rafat[42]);
echo "</br>";
echo "</br>";
var_dump($rafat ["multi"],["dimensional"],["array"]);

// Note:
// Both square brackets and curly braces can be used interchangeably for accessing array elements.
//e.g. $array[42] and $array{42} will both do the same thing in the example above.

?>
