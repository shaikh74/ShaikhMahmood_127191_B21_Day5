<?php

function double($i)
{
    return $i*2;
}
$b = $a = 5;
echo $b;               //5
echo "<hr>";
$c = $a++;
echo $c;               //5
echo "<hr>";
echo $a;              //6
echo "<hr>";
$e = $d = ++$b;
echo $e;             //6
echo "<hr>";
$f = double($d++);
echo $f;          //18
echo "<hr>";
$g = double(++$e); 
echo $g;      //21
echo "<hr>";
$h = $g += 10; // $h=$g+10 
echo $h;      //31
echo "<hr>";
$k= $h -=3;  //$k=$h-3
echo $k;


