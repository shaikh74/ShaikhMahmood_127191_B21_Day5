<?php
// ++$a	  Pre-increment	    Increments $a by one, then returns $a.
// $a++	  Post-increment    Returns $a, then increments $a by one.
// --$a	  Pre-decrement	    Decrements $a by one, then returns $a.
//$a--	  Post-decrement    Returns $a, then decrements $a by one.


$a=10;
echo $a++;           //10
echo "<hr>";
echo $a;                 //11
echo "<hr>";
echo $a++;                //11
echo "<hr>";

echo ++$a;            //13
echo "<hr>";

echo --$a;               //12





