<?php

$a=-5;

$b=-15;

$c= -2; // -2,2

$d=22;

$x=$b%$c;

$y=$a%$d;

$z= $d%$a;
        
echo "The 1st outcome is $x ";
echo "<hr>";
echo "The 2nd outcome is $y";
echo "<hr>";
echo "The 3rd outcome is $z";

/* The result of the modulus operator % has the same sign as the dividend 
 *  that is, the result of $a % $b will have the same sign as $a. For example:
 *  echo (5 % 3)."\n";           // prints 2
    echo (5 % -3)."\n";          // prints 2
    echo (-5 % 3)."\n";          // prints -2
    echo (-5 % -3)."\n";         // prints -2

 */





?>

